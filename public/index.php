<?php

header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	
    // Массив для временного хранения сообщений пользователю.
  $messages = array();


  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
  }


  // Складываем признак ошибок в массив.
  $errors = array();
  
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  // $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['abilities'] = !empty($_COOKIE['abilities_error']);
  $errors['bio'] = !empty($_COOKIE['bio_error']);
  $errors['accept'] = !empty($_COOKIE['accept_error']);



  // Выдаем сообщения об ошибках.
  if ($errors['fio']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('fio_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните имя.</div>';
  }
  if ($errors['email']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('email_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните почту.</div>';
    // $messages[] = '<div class="error">Неправильно введенная почта, проверьте написание еще раз.</div>';
  }
  if ($errors['sex']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('sex_error', '', 100000);
    if ($_COOKIE['sex_error'] != '1' and $_COOKIE['sex_error'] != '0') {
      $messages[] = '<div class="error">Заполните пол.</div>';
    }
    // Выводим сообщение.
    // $messages[] = '<div class="error">Заполните пол.</div>';
  }
  if ($errors['abilities']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('abilities_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните возможность.</div>';
  } 
  if ($errors['bio']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('bio_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните биографию.</div>';
  }
  if ($errors['accept']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('accept_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Поставьте галочку.</div>';
  }


  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();

  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
 	$values['abilities'] = empty($_COOKIE['abilities_value']) ? '' : $_COOKIE['abilities_value'];
 	$values['bio'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];
  $values['accept'] = empty($_COOKIE['accept_value']) ? '' : $_COOKIE['accept_value'];


  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');

  // Завершаем работу скрипта.

  exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {






	$ability_data = ['god', 'idclip', 'levitation'];
	$errors = FALSE;

	if (empty($_POST['fio'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на год.
    setcookie('fio_value', $_POST['fio'], time() + 12 * 30 * 24 * 60 * 60);
  }

  if (empty($_POST['email'])) {
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ) {
    setcookie('email_error_incorrect', '1', time() + 24 * 60 * 60);
    $errors = TRUE; 
  }
  else {
    setcookie('email_value', $_POST['email'], time() + 12 * 30 * 24 * 60 * 60);
  }

  if (empty($_POST['year'])) {
    setcookie('year_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('year_value', $_POST['year'], time() + 12 * 30 * 24 * 60 * 60);
  }

  $sex_answer = $_POST['sex'];
  if ($sex_answer != '0' and $sex_answer != '1') {
    setcookie('sex_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  // if (empty($_POST['sex'])) {
  //   setcookie('sex_error', '1', time() + 24 * 60 * 60);
  //   $errors = TRUE;
  // }
  else {
    setcookie('sex_value', $_POST['sex'], time() + 12 * 30 * 24 * 60 * 60);
  }

  if (empty($_POST['abilities'])) {
    setcookie('abilities_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('abilities_value', $_POST['abilities'], time() + 12 * 30 * 24 * 60 * 60);
  }

  if (empty($_POST['bio'])) {
    setcookie('bio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('bio_value', $_POST['bio'], time() + 12 * 30 * 24 * 60 * 60);
  }

  // if (empty($_POST['accept'])) {
  //   setcookie('accept_error', '1', time() + 24 * 60 * 60);
  //   $errors = TRUE;
  // }



  if (!isset($_POST['accept'])) {
    setcookie('accept_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('accept_value', $_POST['accept'], time() + 12 * 30 * 24 * 60 * 60);
  }


  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');


  $ability_insert = [];

	foreach ($ability_data as $ability){
    $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;
	}

  if ($errors) {
  	// При наличии ошибок завершаем работу скрипта.
  	exit();
	}


  // Сохранение в базу данных.

	$user = 'u20382';
	$pass = '6479868';
	$db = new PDO('mysql:host=localhost;dbname=u20382', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

	// Подготовленный запрос. Не именованные метки.
	try {
  	$stmt = $db->prepare("INSERT INTO application SET name = ?, year = ?, sex = ?, ability_god = ?, ability_idclip = ?, ability_levitation = ?, biography = ?, accept = ? ");
  
  	$stmt -> execute([$_POST['fio'], intval($_POST['year']),  $_POST['sex'], $ability_insert['god'], $ability_insert['idclip'], $ability_insert['levitation'], $_POST['bio'], $_POST['accept'] ]);
	}
	catch(PDOException $e){
  	print('Error : ' . $e->getMessage()); 
  	exit();
	}



  header('Location: ?save=1');
}

// ТУТ НАЧИНАЕТСЯ СТАРЫЙ КОД ИЗ МОЕЙ ПРОШЛОЙ ЛАБЫ
 
// $ability_data = ['god', 'idclip', 'levitation'];

// // Проверяем ошибки.
// $errors = FALSE;
// if (empty($_POST['fio'])) {
//   print('Заполните имя.<br/>');
//   $errors = TRUE;
// }
// else if (!preg_match('/^[a-zA-Z]+$/u', $_POST['fio'])) {
//     print('Вы ввели несуществующие имя.<br/>');
//     $errors = TRUE;
   
// }
// else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) { 
//     print('Вы ввели несуществующие e-mail.<br/>');
//     $errors = TRUE;
// }
// else if (empty($_POST['year'])){
//     print("Вы не выбрали год");
//     $errors = TRUE;
// }
// else if (empty($_POST['sex'])){
//     print("Вы не заполнили пол");
//     $errors = TRUE;
// }
// else if (empty($_POST['abilities'])){
//     print("Вы не заполнили суперспособность");
//     $errors = TRUE;
// }
// else if (empty($_POST['bio'])) {
//     print('Заполните биографию.<br/>');
//     $errors = TRUE;
// }
// else if (empty($_POST['accept'])){
//     print("Вы не приняли условия!");
//     $errors = TRUE;
// } else {
//     $abilities = $_POST['abilities'];
//     foreach ($abilities as $ability){
//         if (!in_array($ability, ['god', 'idclip', 'levitation'])){
//             print("Вы выбрали не ту способность");
//             $errors = TRUE;
//         }
//     }
// }

// $ability_insert = [];

// foreach ($ability_data as $ability){
//     $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;
// }

// if ($errors) {
//   // При наличии ошибок завершаем работу скрипта.
//   exit();
// }

// // Сохранение в базу данных.

// $user = 'u20382';
// $pass = '6479868';
// $db = new PDO('mysql:host=localhost;dbname=u20382', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

// // Подготовленный запрос. Не именованные метки.
// try {
//   $stmt = $db->prepare("INSERT INTO application SET name = ?, year = ?, sex = ?, ability_god = ?, ability_idclip = ?, ability_levitation = ?, biography = ?, accept = ? ");
  
//   $stmt -> execute([$_POST['fio'], intval($_POST['year']),  $_POST['sex'], $ability_insert['god'], $ability_insert['idclip'], $ability_insert['levitation'], $_POST['bio'], $_POST['accept'] ]);
// }
// catch(PDOException $e){
//   print('Error : ' . $e->getMessage()); 
//   exit();
// }


// // Делаем перенаправление.
// // Если запись не сохраняется, но ошибок не видно, то можно закомментировать эту строку чтобы увидеть ошибку.
// // Если ошибок при этом не видно, то необходимо настроить параметр display_errors для PHP.
// header('Location: ?save=1');
