<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="generator" content="GitLab Pages">
		<title>task 4 of backend course by Sinitsa</title>
		<link rel="stylesheet" href="style.css">
		<style>
			
			/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
			.error {
			border: 2px solid red;
			}
		</style>
	</head>
	<body>
		
		<div class="center">
			
			<form action="index.php" method="POST">
				
				Ваше имя?<br>
				<input size='40' type='text' maxlength='25' name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>"> <br>
				
				Ваш email?<br>
				<input size="60" name="email" type="text" maxlength="50" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>"><br>
				Выберите год рождения?<br>
				<select name="year">
					<?php for ($i = 1900; $i < 2020; $i++){ ?>
					<option value=<?php print $i; ?>><?= $i; ?></option>
					<?php  } ?>
				</select><br>
				
				<div <?php if ($errors['sex']) {print 'class="error"';} ?> value="<?php print $values['sex']; ?>">Выберите пол?<br>
					<label class="radio">
						<input type="radio" name="sex" value='0'>
						Мужской
					</label>
					<label class="radio">
						<input type="radio" name="sex" value='1'>
						Женский
					</label>
				</div>
				<div <?php if ($errors['abilities']) {print 'class="error"';} ?> value="<?php print $values['abilities']; ?>">Какие у вас сверхспособности?<br>
					<select multiple name="abilities[]">
						<option value="god">бессмертие</option>
						<option value="idclip">прохождение сквозь стены</option>
						<option value="levitation">левитация</option>
					</select><br>
				</div>
				
				<div <?php if ($errors['bio']) {print 'class="error"';} ?> value="<?php print $values['bio']; ?>">Ваша биография?<br>
					<textarea rows="10" cols="40" name="bio" ></textarea>
					
					С контрактом ознакомлен:
					<input type="checkbox" name="accept" value=1 <?php if ($errors['accept']) {print 'class="error"';} ?> value="<?php print $values['accept']; ?>">Ознакомлен<br>
				</div>
				<input type="submit" value="ok" />
				
				<?php
				if (!empty($messages)) {
				print('<div id="messages">');
					// Выводим все сообщения.
					foreach ($messages as $message) {
					print($message);
					}
				print('</div>');
				}
				?>
			</form>
		</div>
	</body>
</html>